﻿/* d3dfont.h */

#ifndef _FONT_H
#define _FONT_H

#ifdef __cplusplus
extern "C" {
#endif

#include <tchar.h>
#include <stdint.h>

#include "renderer.h"

#define D3DFONT_BOLD		0x1
#define D3DFONT_ITALIC		0x2
#define D3DFONT_ZENABLE		0x4

#define D3DFONT_CENTERED	0x1
#define D3DFONT_TWOSIDED	0x2
#define D3DFONT_FILTERED	0x4
#define DT_SHADOW			0x8

typedef TCHAR tchar_t;

typedef struct
{
	tchar_t m_font_name[64];
	uint32_t m_font_height;
	uint32_t m_font_weight;
	uint32_t m_font_flags;

	/* a d3d device used for rendering */
	IDirect3DDevice9* m_d3d_device;
	/* the d3d texture for thisptr font */
	IDirect3DTexture9* m_texture;
	/* vertex buffer for rendering text */
	IDirect3DVertexBuffer9* m_vertex_buffer;
	/* texture dimensions */
	uint32_t m_tex_width;
	uint32_t m_tex_height;
	float m_text_scale;
	float m_tex_coords[128 - ' '][4];
	/* character pixel spacing per side */
	uint32_t m_spacing;

	/* stateblocks for setting and restoring render states */
	IDirect3DStateBlock9* m_saved_state_block;
	IDirect3DStateBlock9* m_draw_text_state_block;
}
d3d_font_t;


extern void init_d3d_font(d3d_font_t* thisptr, tchar_t* strFontName, uint32_t dwHeight, uint32_t dwFontWeight, uint32_t dwFlags);
extern void release_d3d_font(d3d_font_t* thisptr);

// 2D and 3D text drawing functions
HRESULT draw_text(d3d_font_t* thisptr, float x, float y, uint32_t dwColor,
	tchar_t* strText, uint32_t dwFlags);

// Function to get extent of text
HRESULT get_text_extent(d3d_font_t* thisptr, tchar_t* strText, SIZE* pSize);


HRESULT init_device_objects(d3d_font_t* thisptr, LPDIRECT3DDEVICE9 pd3dDevice);
HRESULT restore_device_objects(d3d_font_t* thisptr);
HRESULT invalidate_device_objects(d3d_font_t* thisptr);
HRESULT delete_device_objects(d3d_font_t* thisptr);

#ifdef __cplusplus
}
#endif

#endif /* _FONT_H */
