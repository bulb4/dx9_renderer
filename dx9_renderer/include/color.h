/* color.h */

#ifndef _COLOR_H
#define _COLOR_H

#ifdef __cplusplus
extern "C" {
#endif

#include <d3d9.h>
#pragma comment(lib,"d3d9.lib")

//rgba float[4] to color_t, it should be faster than color_t(float*) constructor 
#define PF2COL(color) (D3DCOLOR)( \
(((color[3] * 255.f) & 0xff) << 24) | (((color[0] * 255.f) & 0xff) << 16) | \
(((color[1] * 255.f) & 0xff) << 8) | ((color[2] * 255.f) & 0xff))

#define colors_Black	0xFF000000
#define colors_White	0xFFFFFFFF
#define colors_Red		0xFFFF0000
#define colors_Green	0xFF00FF00
#define colors_Blue		0xFF0000FF
#define colors_Yellow	0xFFFFFF00
#define colors_SkyBlue	0xFF00FFFF
#define colors_Pink		0xFFFF00FF


#ifdef __cplusplus
}
#endif

#endif /* _COLOR_H */
