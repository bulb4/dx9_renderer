﻿/* d3dfont.c */

#ifdef __cplusplus
extern "C" {
#endif

#include <stdio.h>
#include <math.h>

#include "d3d_font.h"


#define MAX_NUM_VERTICES (50 * 6)

typedef struct
{
	struct
	{
		float x, y, z, w;
	}
	p;
	D3DCOLOR color;
	float tu, tv;
}
font_vertex_t;

inline font_vertex_t init_font_vertex(float x, float y, D3DCOLOR color, float tu, float tv)
{
	font_vertex_t v =
	{
		{
			x,
			y,
			//0.9f,
			//1.f
		},
		color,
		tu,
		tv
	};

	return(v);
}

void init_d3d_font(d3d_font_t* thisptr, tchar_t* strFontName, uint32_t dwHeight, uint32_t dwFontWeight, uint32_t dwFlags)
{
	memcpy(thisptr->m_font_name, strFontName, sizeof(thisptr->m_font_name) / sizeof(tchar_t));
	thisptr->m_font_height = dwHeight;
	thisptr->m_font_weight = dwFontWeight;
	thisptr->m_font_flags = dwFlags;

	thisptr->m_d3d_device = NULL;
	thisptr->m_texture = NULL;
	thisptr->m_vertex_buffer = NULL;
	thisptr->m_saved_state_block = NULL;
	thisptr->m_draw_text_state_block = NULL;
}

void release_d3d_font(d3d_font_t* thisptr)
{
	invalidate_device_objects(thisptr);
	delete_device_objects(thisptr);
}

HRESULT init_device_objects(d3d_font_t* thisptr, LPDIRECT3DDEVICE9 pd3dDevice)
{
	thisptr->m_d3d_device = pd3dDevice;

	// Establish the font and texture size
	thisptr->m_text_scale = 1.f; // Draw fonts into texture without scaling

	thisptr->m_tex_width = thisptr->m_tex_height =
		thisptr->m_font_height > 60 ? 2048 :
		thisptr->m_font_height > 30 ? 1024 :
		thisptr->m_font_height > 15 ? 512 : 256;


	// If requested texture is too big, use a smaller texture and smaller font,
	// and scale up when rendering.
	D3DCAPS9 d3dCaps;
	IDirect3DDevice9_GetDeviceCaps(thisptr->m_d3d_device, &d3dCaps);

	if (thisptr->m_tex_width > d3dCaps.MaxTextureWidth)
	{
		thisptr->m_text_scale = (float)d3dCaps.MaxTextureWidth / (float)thisptr->m_tex_width;
		thisptr->m_tex_width = thisptr->m_tex_height = d3dCaps.MaxTextureWidth;
	}

	// Create a new texture for the font
	HRESULT hr = IDirect3DDevice9_CreateTexture(thisptr->m_d3d_device,
		thisptr->m_tex_width,
		thisptr->m_tex_height,
		1, 0, D3DFMT_A4R4G4B4,
		D3DPOOL_MANAGED, &thisptr->m_texture, NULL);

	if (FAILED(hr))
		return(hr);

	// Prepare to create a bitmap
	uint32_t*      pBitmapBits;
	BITMAPINFO bmi;
	ZeroMemory(&bmi.bmiHeader, sizeof(BITMAPINFOHEADER));
	bmi.bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
	bmi.bmiHeader.biWidth = (int)thisptr->m_tex_width;
	bmi.bmiHeader.biHeight = -(int)thisptr->m_tex_height;
	bmi.bmiHeader.biPlanes = 1;
	bmi.bmiHeader.biCompression = BI_RGB;
	bmi.bmiHeader.biBitCount = 32;

	// Create a DC and a bitmap for the font
	HDC     h_dc = CreateCompatibleDC(NULL);
	HBITMAP hbmBitmap = CreateDIBSection(h_dc, &bmi, DIB_RGB_COLORS,
		(VOID**)&pBitmapBits, NULL, 0);
	SetMapMode(h_dc, MM_TEXT);

	// Create a font.  By specifying ANTIALIASED_QUALITY, we might get an
	// antialiased font, but thisptr is not guaranteed.
	INT nHeight = -MulDiv(thisptr->m_font_height,
		(INT)(GetDeviceCaps(h_dc, LOGPIXELSY) * thisptr->m_text_scale), 72);

	HFONT hFont = CreateFont(nHeight, 0, 0, 0, thisptr->m_font_weight,
		thisptr->m_font_flags & D3DFONT_ITALIC ? TRUE : FALSE,
		FALSE, FALSE, DEFAULT_CHARSET, OUT_DEFAULT_PRECIS,
		CLIP_DEFAULT_PRECIS, ANTIALIASED_QUALITY,
		VARIABLE_PITCH, thisptr->m_font_name);

	if (!hFont)
		return(E_FAIL);

	SelectObject(h_dc, hbmBitmap);
	SelectObject(h_dc, hFont);

	// Set text properties
	SetTextColor(h_dc, RGB(255, 255, 255));
	SetBkColor(h_dc, 0);
	SetTextAlign(h_dc, TA_TOP);

	// Loop through all printable character and output them to the bitmap..
	// Meanwhile, keep track of the corresponding tex coords for each character.
	uint32_t x = 0;
	uint32_t y = 0;
	tchar_t str[2] = _T("x");
	SIZE size;

	// Calculate the spacing between characters based on line height
	GetTextExtentPoint32(h_dc, TEXT(" "), 1, &size);
	x = thisptr->m_spacing = (uint32_t)ceilf(size.cy * 0.3f);

	for (tchar_t c = ' '; c < 127; c++)
	{
		str[0] = c;
		GetTextExtentPoint32(h_dc, str, 1, &size);

		if ((uint32_t)(x + size.cx + 1) > thisptr->m_tex_width)
		{
			x = thisptr->m_spacing;
			y += size.cy + 1;
		}

		ExtTextOut(h_dc, x, y, ETO_OPAQUE, NULL, str, 1, NULL);

		thisptr->m_tex_coords[c - ' '][0] = (float)(x - thisptr->m_spacing) / thisptr->m_tex_width;
		thisptr->m_tex_coords[c - ' '][1] = (float)(y) / thisptr->m_tex_height;
		thisptr->m_tex_coords[c - ' '][2] = (float)(x + size.cx + thisptr->m_spacing) / thisptr->m_tex_width;
		thisptr->m_tex_coords[c - ' '][3] = (float)(y + size.cy) / thisptr->m_tex_height;

		x += size.cx + (thisptr->m_spacing * 2);
	}

	// Lock the surface and write the alpha values for the set pixels
	D3DLOCKED_RECT d3dlr;
	IDirect3DTexture9_LockRect(thisptr->m_texture, 0, &d3dlr, 0, 0);
	BYTE* pDstRow = (BYTE*)d3dlr.pBits;
	WORD* pDst16;
	BYTE bAlpha; // 4-bit measure of pixel intensity

	for (y = 0; y < thisptr->m_tex_height; y++)
	{
		pDst16 = (WORD*)pDstRow;

		for (x = 0; x < thisptr->m_tex_width; x++)
		{
			bAlpha = (BYTE)((pBitmapBits[thisptr->m_tex_width * y + x] & 0xff) >> 4);
			*pDst16++ = bAlpha > 0 ? ((bAlpha << 12) | 0x0fff) : 0;
		}

		pDstRow += d3dlr.Pitch;
	}

	/* done updating texture, so clean up used objects */
	IDirect3DTexture9_UnlockRect(thisptr->m_texture, 0);
	DeleteObject(hbmBitmap);
	DeleteDC(h_dc);
	DeleteObject(hFont);

	return(S_OK);
}

HRESULT restore_device_objects(d3d_font_t* thisptr)
{
	HRESULT hr;

	// Create vertex buffer for the letters
	if (FAILED(hr = IDirect3DDevice9_CreateVertexBuffer(
		thisptr->m_d3d_device,
		MAX_NUM_VERTICES * sizeof(font_vertex_t),
		D3DUSAGE_WRITEONLY, 0, D3DPOOL_DEFAULT,
		&thisptr->m_vertex_buffer, NULL)))
	{
		return(hr);
	}

	/* create the state blocks for rendering text */
	for (int which = 0; which < 2; which++)
	{
		IDirect3DDevice9_BeginStateBlock(thisptr->m_d3d_device);
		IDirect3DDevice9_SetTexture(thisptr->m_d3d_device, 0,
			(IDirect3DBaseTexture9*)thisptr->m_texture);

#define set_render_state(state, value) \
IDirect3DDevice9_SetRenderState( \
thisptr->m_d3d_device, state, value)

		set_render_state(D3DRS_ZENABLE,
			D3DFONT_ZENABLE & thisptr->m_font_flags ? TRUE : FALSE);

		set_render_state(D3DRS_ALPHABLENDENABLE, TRUE);
		set_render_state(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
		set_render_state(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);
		set_render_state(D3DRS_ALPHATESTENABLE, TRUE);
		set_render_state(D3DRS_ALPHAREF, 0x08);
		set_render_state(D3DRS_ALPHAFUNC, D3DCMP_GREATEREQUAL);
		set_render_state(D3DRS_FILLMODE, D3DFILL_SOLID);
		set_render_state(D3DRS_CULLMODE, D3DCULL_CCW);
		set_render_state(D3DRS_STENCILENABLE, FALSE);
		set_render_state(D3DRS_CLIPPING, TRUE);
		set_render_state(D3DRS_CLIPPLANEENABLE, FALSE);
		set_render_state(D3DRS_VERTEXBLEND, FALSE);
		set_render_state(D3DRS_INDEXEDVERTEXBLENDENABLE, FALSE);
		set_render_state(D3DRS_FOGENABLE, FALSE);

#undef set_render_state

#define set_texture_stage_state(stage, type, value) \
IDirect3DDevice9_SetTextureStageState( \
thisptr->m_d3d_device,stage, type, value)

		set_texture_stage_state(0, D3DTSS_COLOROP, D3DTOP_MODULATE);
		set_texture_stage_state(0, D3DTSS_COLORARG1, D3DTA_TEXTURE);
		set_texture_stage_state(0, D3DTSS_COLORARG2, D3DTA_DIFFUSE);
		set_texture_stage_state(0, D3DTSS_ALPHAOP, D3DTOP_MODULATE);
		set_texture_stage_state(0, D3DTSS_ALPHAARG1, D3DTA_TEXTURE);
		set_texture_stage_state(0, D3DTSS_ALPHAARG2, D3DTA_DIFFUSE);
		set_texture_stage_state(0, D3DTSS_TEXCOORDINDEX, 0);
		set_texture_stage_state(0, D3DTSS_TEXTURETRANSFORMFLAGS, D3DTTFF_DISABLE);
		set_texture_stage_state(1, D3DTSS_COLOROP, D3DTOP_DISABLE);
		set_texture_stage_state(1, D3DTSS_ALPHAOP, D3DTOP_DISABLE);

#undef set_texture_stage_state

		IDirect3DDevice9_SetSamplerState(thisptr->m_d3d_device, 0, D3DSAMP_MINFILTER, D3DTEXF_POINT);
		IDirect3DDevice9_SetSamplerState(thisptr->m_d3d_device, 0, D3DSAMP_MAGFILTER, D3DTEXF_POINT);
		IDirect3DDevice9_SetSamplerState(thisptr->m_d3d_device, 0, D3DSAMP_MIPFILTER, D3DTEXF_NONE);

		IDirect3DDevice9_EndStateBlock(thisptr->m_d3d_device,
			which == 0 ?
			&thisptr->m_saved_state_block :
			&thisptr->m_draw_text_state_block);
	}

	return(S_OK);
}

HRESULT invalidate_device_objects(d3d_font_t* thisptr)
{
	IDirect3DVertexBuffer9_Release(thisptr->m_vertex_buffer);

	// Delete the state blocks
	if (thisptr->m_d3d_device)
	{
		if (thisptr->m_saved_state_block)
			IDirect3DStateBlock9_Release(thisptr->m_saved_state_block);

		if (thisptr->m_draw_text_state_block)
			IDirect3DStateBlock9_Release(thisptr->m_draw_text_state_block);
	}

	thisptr->m_saved_state_block = NULL;
	thisptr->m_draw_text_state_block = NULL;

	return(S_OK);
}

HRESULT delete_device_objects(d3d_font_t* thisptr)
{
	IDirect3DTexture9_Release(thisptr->m_texture);
	thisptr->m_d3d_device = NULL;

	return(S_OK);
}

HRESULT get_text_extent(d3d_font_t* thisptr, tchar_t* strText, SIZE* pSize)
{
	if (!strText || !pSize)
		return(E_FAIL);

	float fRowWidth = 0.f;
	float fRowHeight = (
		thisptr->m_tex_coords[0][3] -
		thisptr->m_tex_coords[0][1]) *
		thisptr->m_tex_height;

	float fWidth = 0.f;
	float fHeight = fRowHeight;

	while (*strText)
	{
		tchar_t c = *strText++;

		if (c == _T('\n'))
		{
			fRowWidth = 0.0f;
			fHeight += fRowHeight;
		}

		if ((c - ' ') < 0 ||
			(c - ' ') >= 128 - ' ')
			continue;

		fRowWidth += (
			thisptr->m_tex_coords[c - ' '][2] -
			thisptr->m_tex_coords[c - ' '][0]) *
			thisptr->m_tex_width - 2 * thisptr->m_spacing;

		if (fRowWidth > fWidth)
			fWidth = fRowWidth;
	}

	pSize->cx = (int)fWidth;
	pSize->cy = (int)fHeight;

	return(S_OK);
}

HRESULT draw_text(d3d_font_t* thisptr, float sx, float sy, uint32_t dwColor,
	tchar_t* strText, uint32_t dwFlags)
{
	IDirect3DStateBlock9_Capture(thisptr->m_saved_state_block);
	IDirect3DStateBlock9_Apply(thisptr->m_draw_text_state_block);
	IDirect3DDevice9_SetFVF(thisptr->m_d3d_device, (D3DFVF_XYZRHW | D3DFVF_DIFFUSE | D3DFVF_TEX1));
	IDirect3DDevice9_SetPixelShader(thisptr->m_d3d_device, NULL);
	IDirect3DDevice9_SetStreamSource(thisptr->m_d3d_device, 0, thisptr->m_vertex_buffer, 0, sizeof(font_vertex_t));

	if (dwFlags & D3DFONT_FILTERED)
	{
		IDirect3DDevice9_SetSamplerState(thisptr->m_d3d_device, 0, D3DSAMP_MINFILTER, D3DTEXF_LINEAR);
		IDirect3DDevice9_SetSamplerState(thisptr->m_d3d_device, 0, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR);
	}

	if (dwFlags & DT_CENTER)
	{
		struct { long x, y; } size;
		get_text_extent(thisptr, strText, (SIZE*)&size);

		sx -= (float)size.x / 2.f;
		sy -= (float)size.y / 2.f;
	}

	/* adjust for character spacing */

	sx -= thisptr->m_spacing;
	float fStartX = sx;

	// fill vertex buffer
	font_vertex_t* pVertices = NULL;
	uint32_t dwNumTriangles = 0;
	IDirect3DVertexBuffer9_Lock(thisptr->m_vertex_buffer, 0, 0, (VOID**)&pVertices, D3DLOCK_DISCARD);

	while (*strText)
	{
		tchar_t c = *strText++;

		if (c == _T('\n'))
		{
			sx = fStartX;
			sy += (thisptr->m_tex_coords[0][3] - thisptr->m_tex_coords[0][1]) * thisptr->m_tex_height;
			continue;
		}

		if ((c - ' ') < 0 ||
			(c - ' ') >= 128 - ' ')
			continue;

		float tx1 = thisptr->m_tex_coords[c - ' '][0];
		float ty1 = thisptr->m_tex_coords[c - ' '][1];
		float tx2 = thisptr->m_tex_coords[c - ' '][2];
		float ty2 = thisptr->m_tex_coords[c - ' '][3];

		float w = (tx2 - tx1) * thisptr->m_tex_width / thisptr->m_text_scale;
		float h = (ty2 - ty1) * thisptr->m_tex_height / thisptr->m_text_scale;

		if (c != _T(' '))
		{
/* init font vertex */
#define ifv(x, y, col, u, v) \
*pVertices++ = init_font_vertex( \
x, y, col, u, v)

			if (dwFlags & DT_SHADOW)
			{
				float sxa = sx + 1.f;
				float sya = sy;

				ifv(sxa + 0 - 0.5f, sy + h - 0.5f, 0xff000000, tx1, ty2);
				ifv(sxa + 0 - 0.5f, sy + 0 - 0.5f, 0xff000000, tx1, ty1);
				ifv(sxa + w - 0.5f, sy + h - 0.5f, 0xff000000, tx2, ty2);
				ifv(sxa + w - 0.5f, sy + 0 - 0.5f, 0xff000000, tx2, ty1);
				ifv(sxa + w - 0.5f, sy + h - 0.5f, 0xff000000, tx2, ty2);
				ifv(sxa + 0 - 0.5f, sy + 0 - 0.5f, 0xff000000, tx1, ty1);

				sxa = sx - 1.f;
				ifv(sxa + 0 - 0.5f, sy + h - 0.5f, 0xff000000, tx1, ty2);
				ifv(sxa + 0 - 0.5f, sy + 0 - 0.5f, 0xff000000, tx1, ty1);
				ifv(sxa + w - 0.5f, sy + h - 0.5f, 0xff000000, tx2, ty2);
				ifv(sxa + w - 0.5f, sy + 0 - 0.5f, 0xff000000, tx2, ty1);
				ifv(sxa + w - 0.5f, sy + h - 0.5f, 0xff000000, tx2, ty2);
				ifv(sxa + 0 - 0.5f, sy + 0 - 0.5f, 0xff000000, tx1, ty1);

				sya = sy - 1.f;
				ifv(sx + 0 - 0.5f, sya + h - 0.5f, 0xff000000, tx1, ty2);
				ifv(sx + 0 - 0.5f, sya + 0 - 0.5f, 0xff000000, tx1, ty1);
				ifv(sx + w - 0.5f, sya + h - 0.5f, 0xff000000, tx2, ty2);
				ifv(sx + w - 0.5f, sya + 0 - 0.5f, 0xff000000, tx2, ty1);
				ifv(sx + w - 0.5f, sya + h - 0.5f, 0xff000000, tx2, ty2);
				ifv(sx + 0 - 0.5f, sya + 0 - 0.5f, 0xff000000, tx1, ty1);

				sya = sy + 1.f;
				ifv(sx + 0 - 0.5f, sya + h - 0.5f, 0xff000000, tx1, ty2);
				ifv(sx + 0 - 0.5f, sya + 0 - 0.5f, 0xff000000, tx1, ty1);
				ifv(sx + w - 0.5f, sya + h - 0.5f, 0xff000000, tx2, ty2);
				ifv(sx + w - 0.5f, sya + 0 - 0.5f, 0xff000000, tx2, ty1);
				ifv(sx + w - 0.5f, sya + h - 0.5f, 0xff000000, tx2, ty2);
				ifv(sx + 0 - 0.5f, sya + 0 - 0.5f, 0xff000000, tx1, ty1);

				dwNumTriangles += 8;
			}

			ifv(sx + 0 - 0.5f, sy + h - 0.5f, dwColor, tx1, ty2);
			ifv(sx + 0 - 0.5f, sy + 0 - 0.5f, dwColor, tx1, ty1);
			ifv(sx + w - 0.5f, sy + h - 0.5f, dwColor, tx2, ty2);
			ifv(sx + w - 0.5f, sy + 0 - 0.5f, dwColor, tx2, ty1);
			ifv(sx + w - 0.5f, sy + h - 0.5f, dwColor, tx2, ty2);
			ifv(sx + 0 - 0.5f, sy + 0 - 0.5f, dwColor, tx1, ty1);
			dwNumTriangles += 2;

#undef ifv

			/* adapt calculation so it works with shadowing also */
			if (dwNumTriangles * 3 > (MAX_NUM_VERTICES - (dwNumTriangles * 3)))
			{
				/* unlock, render, and relock the vertex buffer */
				IDirect3DVertexBuffer9_Unlock(thisptr->m_vertex_buffer);
				IDirect3DDevice9_DrawPrimitive(thisptr->m_d3d_device, D3DPT_TRIANGLELIST, 0, dwNumTriangles);
				pVertices = NULL;
				IDirect3DVertexBuffer9_Lock(thisptr->m_vertex_buffer,
					0, 0, (VOID**)&pVertices, D3DLOCK_DISCARD);

				dwNumTriangles = 0;
			}
		}

		sx += w - (thisptr->m_spacing * 2);
	}

	/* unlock and render the vertex buffer */
	IDirect3DVertexBuffer9_Unlock(thisptr->m_vertex_buffer);

	if (dwNumTriangles)
		IDirect3DDevice9_DrawPrimitive(thisptr->m_d3d_device, D3DPT_TRIANGLELIST, 0, dwNumTriangles);

	/* restore the modified renderstates */
	IDirect3DStateBlock9_Apply(thisptr->m_saved_state_block);

	return(S_OK);
}

#ifdef __cplusplus
}
#endif
