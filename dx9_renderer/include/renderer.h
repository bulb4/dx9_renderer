/* renderer.h */

#ifndef _RENDERER_H
#define _RENDERER_H

#ifdef __cplusplus
extern "C" {
#endif

#ifdef USE_FLOAT_COORD
	typedef float coord_t;
#else
	typedef int coord_t;
#endif
	
#include <d3d9.h>
#pragma comment(lib, "d3d9.lib")

#include "d3d_font.h"
#include "color.h"
	
typedef enum
{
	RenderDrawType_None = 0,
	RenderDrawType_Outlined = 1 << 0,
	RenderDrawType_Filled = 1 << 1,
	RenderDrawType_Gradient = 1 << 2,
	RenderDrawType_OutlinedGradient = RenderDrawType_Outlined | RenderDrawType_Gradient,
	RenderDrawType_FilledGradient = RenderDrawType_Filled | RenderDrawType_Gradient
}
RenderDrawType;

typedef struct
{
	void(*begin_draw)();
	void(*end_draw)();

	void(*on_lost_device)();
	void(*on_reset_device)();

	void(*draw_line)(coord_t, coord_t, coord_t, coord_t, D3DCOLOR);
	void(*draw_filled_box)(coord_t, coord_t, coord_t, coord_t, D3DCOLOR);
	void(*draw_box)(coord_t, coord_t, coord_t, coord_t, coord_t, D3DCOLOR);
	void(*draw_gradient_box)(coord_t, coord_t, coord_t, coord_t, D3DCOLOR, D3DCOLOR, D3DCOLOR, D3DCOLOR);
	void(*draw_circle)(coord_t, coord_t, coord_t, unsigned, RenderDrawType, D3DCOLOR);
	void(*draw_circle_sector)(coord_t, coord_t, coord_t, unsigned, float, float, D3DCOLOR, D3DCOLOR);
	void(*draw_ring)(coord_t, coord_t, coord_t, coord_t, unsigned, RenderDrawType, D3DCOLOR, D3DCOLOR);
	void(*draw_ring_sector)(coord_t, coord_t, coord_t, coord_t, unsigned, unsigned, unsigned, D3DCOLOR, D3DCOLOR);
	void(*draw_triangle)(coord_t, coord_t, coord_t, coord_t, coord_t, coord_t, RenderDrawType, D3DCOLOR, D3DCOLOR, D3DCOLOR);
} renderer_table_t;

extern renderer_table_t* renderer;

extern HRESULT renderer_initialize(IDirect3DDevice9* p_d3d_device);
extern D3DCOLOR color_hsv(float h, float s, float v, float a);

typedef struct
{
	float x;
	float y;
	float z;
	float rhw;

	D3DCOLOR color;
} vertex_t;

#ifdef __cplusplus
}
#endif

#endif /* _RENDERER_H */
