/* renderer.c */

#ifdef __cplusplus
extern "C" {
#endif

#include <windows.h>
#include <math.h>
#include <malloc.h>

#include "renderer.h"


#define MATH_PI 3.141592f
#define MATH_2PI 6.283185f

static IDirect3DDevice9* p_d3d_device;
static IDirect3DStateBlock9* pStateBlock;

renderer_table_t* renderer = NULL;

inline vertex_t make_vertex(coord_t x, coord_t y, D3DCOLOR color)
{
	vertex_t result =
	{
		(float)x,
		(float)y,
		0.f,
		0.f,
		color
	};

	return(result);
}

D3DCOLOR color_hsv(float h, float s, float v, float a)
{
	h = fmodf(h, 1.f) / (60.f / 360.f);

	const int i = (int)h;
	const float f = h - (float)i;
	const float q = v * (1.f - s * f);
	const float t = v * (1.f - s * (1.0f - f));
	const float p = v * (1.f - s);

	float r, g, b;

	switch (i)
	{
	case 0: r = v; g = t; b = p; break;
	case 1: r = q; g = v; b = p; break;
	case 2: r = p; g = v; b = t; break;
	case 3: r = p; g = q; b = v; break;
	case 4: r = t; g = p; b = v; break;
	default: r = v; g = p; b = q; break;
	}

	return(D3DCOLOR_COLORVALUE(r, g, b, a));
}

void begin_draw()
{		
	//IDirect3DDevice9_CreateStateBlock(p_d3d_device, D3DSBT_ALL, &pStateBlock);
//	IDirect3DDevice9_BeginStateBlock(p_d3d_device);
	
#define set_render_state(state, value) \
IDirect3DDevice9_SetRenderState( \
	p_d3d_device, state, value)

	set_render_state(D3DRS_COLORWRITEENABLE, -1);
	set_render_state(D3DRS_SRGBWRITEENABLE, FALSE);
	set_render_state(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
	set_render_state(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);
	set_render_state(D3DRS_ALPHABLENDENABLE, TRUE);
	set_render_state(D3DRS_ZENABLE, D3DZB_FALSE);
	set_render_state(D3DRS_CULLMODE, D3DCULL_NONE);
	set_render_state(D3DRS_FILLMODE, D3DFILL_SOLID);
	set_render_state(D3DRS_SHADEMODE, D3DSHADE_GOURAUD);
	set_render_state(D3DRS_ZWRITEENABLE, FALSE);
	set_render_state(D3DRS_LASTPIXEL, TRUE);

#undef set_render_state

	IDirect3DDevice9_SetTexture(p_d3d_device, 0, NULL);
	IDirect3DDevice9_SetPixelShader(p_d3d_device, NULL);

	IDirect3DDevice9_SetFVF(p_d3d_device, D3DFVF_XYZRHW | D3DFVF_DIFFUSE);
}

void end_draw()
{
	//IDirect3DDevice9_EndStateBlock(p_d3d_device, &pStateBlock);
}

void on_lost_device()
{

}
void on_reset_device()
{

}

void draw_line(coord_t x1, coord_t y1, coord_t x2, coord_t y2, D3DCOLOR color)
{
	vertex_t vertexes[2];
	vertexes[0] = make_vertex(x1, y1, color);
	vertexes[1] = make_vertex(x2, y2, color);

	IDirect3DDevice9_DrawPrimitiveUP(p_d3d_device, D3DPT_LINELIST, 1, vertexes, sizeof(vertex_t));
}

void draw_filled_box(coord_t x, coord_t y, coord_t width, coord_t height, D3DCOLOR color)
{
	vertex_t vertexes[4];
	vertexes[0] = make_vertex(x, y, color);
	vertexes[1] = make_vertex(x + width, y, color);
	vertexes[2] = make_vertex(x, y + height, color);
	vertexes[3] = make_vertex(x + width, y + height, color);

	IDirect3DDevice9_DrawPrimitiveUP(p_d3d_device,
		D3DPT_TRIANGLESTRIP, 2, vertexes, sizeof(vertex_t));
}

void draw_box(coord_t x, coord_t y, coord_t width, coord_t height, coord_t thickness, D3DCOLOR color)
{
	if (thickness == (coord_t)1)
	{
		vertex_t vertexes[5];

		vertexes[0] = make_vertex(x, y, color);
		vertexes[1] = make_vertex(x + width, y, color);
		vertexes[2] = make_vertex(x + width, y + height, color);
		vertexes[3] = make_vertex(x, y + height, color);
		vertexes[4] = vertexes[0];

		IDirect3DDevice9_DrawPrimitiveUP(p_d3d_device,
			D3DPT_LINESTRIP,
			4, vertexes,
			sizeof(vertex_t));
	}
	else
	{
		draw_filled_box(x, y, width, thickness, color);
		draw_filled_box(x, y, thickness, height, color);
		draw_filled_box(x + width - thickness, y, thickness, height, color);
		draw_filled_box(x, y + height - thickness, width, thickness, color);
	}
}

void draw_gradient_box(coord_t x, coord_t y, coord_t width, coord_t height, D3DCOLOR color1, D3DCOLOR color2, D3DCOLOR color3, D3DCOLOR color4)
{
	vertex_t vertexes[4];
	vertexes[0] = make_vertex(x, y, color1);
	vertexes[1] = make_vertex(x + width, y, color2);
	vertexes[2] = make_vertex(x, y + height, color3);
	vertexes[3] = make_vertex(x + width, y + height, color4);

	IDirect3DDevice9_DrawPrimitiveUP(p_d3d_device,
		D3DPT_TRIANGLESTRIP,
		2, vertexes,
		sizeof(vertex_t));
}

void draw_circle(coord_t x, coord_t y, coord_t radius, unsigned points, RenderDrawType flags, D3DCOLOR color)
{
	const BOOL filled = (flags & RenderDrawType_Filled);
		
	vertex_t* vertexes = (vertex_t*)alloca(sizeof(vertex_t) * (points + 1));

	for (unsigned i = 0; i <= points; i++)
	{
		const float angle = MATH_2PI / points * i;
		vertexes[i] = make_vertex(x + (coord_t)(cosf(angle) * radius), y + (coord_t)(sinf(angle) * radius), color);
		
			
		if (filled)
		{
			/* todo: constant */

			const float angle = MATH_PI / 180.f;

			float flSin = sinf(angle);
			float flCos = cosf(angle);

			vertexes[i].x = (float)(x + flCos * (vertexes[i].x - x) - flSin * (vertexes[i].y - y));
			vertexes[i].y = (float)(y + flSin * (vertexes[i].x - x) + flCos * (vertexes[i].y - y));
		}
	}

	IDirect3DDevice9_DrawPrimitiveUP(p_d3d_device, 
		(flags & RenderDrawType_Outlined) ? D3DPT_LINESTRIP :
		filled ? D3DPT_TRIANGLEFAN : D3DPT_POINTLIST,
		points, vertexes, sizeof(vertex_t));
}

void draw_circle_sector(coord_t x, coord_t y, coord_t radius, unsigned points, float angle1, float angle2, D3DCOLOR color1, D3DCOLOR color2)
{
	angle1 += 270.f;
	angle2 += 270.f;

	if (angle1 > angle2)
		angle2 += 360.f;
	
	vertex_t* vertexes = (vertex_t*)alloca(sizeof(vertex_t) * (points + 2));

	const float stop = MATH_2PI * angle2 / 360.f;
	vertexes[points + 1] = make_vertex(x + (coord_t)(cosf(stop) * radius), y + (coord_t)(sinf(stop) * radius), color1);
	vertexes[0] = make_vertex(x, y, color2);

	float angle = MATH_2PI * angle1 / 360.f;
	const float step = ((MATH_2PI * angle2 / 360.f) - angle) / points;

	for (unsigned i = 1; i != points + 1; angle += step, i++)
		vertexes[i] = make_vertex(x + (coord_t)(cosf(angle) * radius), y + (coord_t)(sinf(angle) * radius), color1);

	IDirect3DDevice9_DrawPrimitiveUP(p_d3d_device,
		D3DPT_TRIANGLEFAN,
		points, vertexes,
		sizeof(vertex_t));
}

void draw_ring(coord_t x, coord_t y, coord_t radius1, coord_t radius2, unsigned points, RenderDrawType flags, D3DCOLOR color1, D3DCOLOR color2)
{
	if (!(flags & RenderDrawType_Gradient))
		color2 = color1;

	if (flags & RenderDrawType_Outlined)
	{
		draw_circle(x, y, radius1, points, RenderDrawType_Outlined, color1);
		draw_circle(x, y, radius2, points, RenderDrawType_Outlined, color2);
		return;
	}

	const unsigned polygons_per_vertex = 4;

	vertex_t* vertexes = (vertex_t*)alloca(sizeof(vertex_t) * (points * polygons_per_vertex));
		
	for (unsigned i = 0; i < points; i++)
	{
		unsigned it = i * polygons_per_vertex;
		
		const float angle1 = MATH_2PI / points * i;
		const float angle2 = MATH_2PI / points * (i + 1);

		vertexes[it + 0] = make_vertex(x + (coord_t)(cosf(angle1) * radius1), y + (coord_t)(sinf(angle1) * radius1), color1);
		vertexes[it + 1] = make_vertex(x + (coord_t)(cosf(angle1) * radius2), y + (coord_t)(sinf(angle1) * radius2), color2);
		vertexes[it + 2] = make_vertex(x + (coord_t)(cosf(angle2) * radius1), y + (coord_t)(sinf(angle2) * radius1), color1);
		vertexes[it + 3] = make_vertex(x + (coord_t)(cosf(angle2) * radius2), y + (coord_t)(sinf(angle2) * radius2), color2);

		for (unsigned a = 0; a < polygons_per_vertex; a++)
		{
			const float angle = MATH_PI / 180.f, flSin = sinf(angle), flCos = cosf(angle);

			vertexes[it].x = (float)(x + flCos * (vertexes[it].x - x) - flSin * (vertexes[it].y - y));
			vertexes[it].y = (float)(y + flSin * (vertexes[it].x - x) + flCos * (vertexes[it].y - y));

			++it;
		}
	}

	--points;

	vertexes[points * polygons_per_vertex + 0] = vertexes[0];
	vertexes[points * polygons_per_vertex + 1] = vertexes[1];

	IDirect3DDevice9_DrawPrimitiveUP(
		p_d3d_device,
		D3DPT_TRIANGLESTRIP,
		points * polygons_per_vertex,
		vertexes, sizeof(vertex_t));
}

void draw_ring_sector(coord_t x, coord_t y, coord_t radius1, coord_t radius2, unsigned points, unsigned angle1, unsigned angle2, D3DCOLOR color1, D3DCOLOR color2)
{
	angle1 += 270;
	angle2 += 270;

	if (angle1 > angle2)
		angle2 += 360;

	const unsigned modifier = 4;

	vertex_t* vertexes = (vertex_t*)alloca(
		sizeof(vertex_t) * points * modifier);

	const float start = MATH_2PI * angle1 / 360.f;
	const float stop = MATH_2PI * angle2 / 360.f;
	const float step = (stop - start) / points;

	/* container for previous iteration values */
	struct { float sin, cos; } sincos[2] =
	{ sinf(start), cosf(start) };

	for (unsigned i = 0; i < points; i++)
	{
		const float temp_angle = start + step * i;

		sincos[!(i % 2)].sin = sinf(temp_angle + step);
		sincos[!(i % 2)].cos = cosf(temp_angle + step);

		const unsigned it = i * modifier;

		vertexes[it + 0] = vertexes[it + 2] = make_vertex(
			x + (coord_t)(sincos[0].cos * radius1),
			y + (coord_t)(sincos[0].sin * radius1), color1);

		vertexes[it + 1] = vertexes[it + 3] = make_vertex(
			x + (coord_t)(sincos[1].cos * radius2),
			y + (coord_t)(sincos[1].sin * radius2), color2);
	}

	--points;

	vertexes[points * modifier + 0] = make_vertex(x + (coord_t)(cosf(stop) * radius1) + 1, y + (coord_t)(sinf(stop) * radius1), color1);
	vertexes[points * modifier + 1] = make_vertex(x + (coord_t)(cosf(stop) * radius2) + 1, y + (coord_t)(sinf(stop) * radius2), color2);

	IDirect3DDevice9_DrawPrimitiveUP(p_d3d_device,
		D3DPT_TRIANGLESTRIP,
		points * modifier,
		vertexes, sizeof(vertex_t));
}

void draw_triangle(coord_t x1, coord_t y1, coord_t x2, coord_t y2, coord_t x3, coord_t y3, RenderDrawType flags, D3DCOLOR color1, D3DCOLOR color2, D3DCOLOR color3)
{
	if (!(flags & RenderDrawType_Gradient))
	{
		color2 = color1;
		color3 = color1;
	}

	vertex_t vertexes[4];
	vertexes[0] = make_vertex(x1, y1, color1);
	vertexes[1] = make_vertex(x2, y2, color2);
	vertexes[2] = make_vertex(x3, y3, color3);
	vertexes[3] = vertexes[0];

	IDirect3DDevice9_DrawPrimitiveUP(p_d3d_device, 
		(flags & RenderDrawType_Outlined) ? D3DPT_LINESTRIP : 
		(flags & RenderDrawType_Filled) ? D3DPT_TRIANGLEFAN : 
		D3DPT_POINTLIST, 3, vertexes, sizeof(vertex_t));
}

HRESULT renderer_initialize(IDirect3DDevice9* d3d_device)
{
	p_d3d_device = d3d_device;

	if (!p_d3d_device)
		return(D3DERR_NOTFOUND);

	static renderer_table_t renderer_table;

	renderer = &renderer_table;

#define init_method(name) renderer->name = name

	init_method(begin_draw);
	init_method(end_draw);

	init_method(on_lost_device);
	init_method(on_reset_device);

	init_method(draw_line);
	init_method(draw_filled_box);
	init_method(draw_box);
	init_method(draw_gradient_box);
	init_method(draw_circle);
	init_method(draw_circle_sector);
	init_method(draw_ring);
	init_method(draw_ring_sector);
	init_method(draw_triangle);

#undef init_field

	return(D3D_OK);
}

#ifdef __cplusplus
}
#endif
