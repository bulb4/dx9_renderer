#define _CRT_SECURE_NO_WARNINGS

#include <Windows.h>
#include <stdio.h>

#define DIRECTINPUT_VERSION 0x0800
#include <d3d9.h>
#pragma comment(lib,"d3d9.lib")
#include <dinput.h>
#include <tchar.h>

#include "../include/renderer.h"

#include "../example/font_bin.h"

static LPDIRECT3DDEVICE9 g_pd3dDevice = NULL;
static D3DPRESENT_PARAMETERS g_d3dpp;

LRESULT WINAPI WndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	switch (msg)
	{
	case WM_DESTROY:
		PostQuitMessage(0);
		return 0;
	}

	return DefWindowProc(hWnd, msg, wParam, lParam);
}

int __stdcall WinMain(HINSTANCE a, HINSTANCE b, LPSTR c , int d)
{
#define WINDOW_NAME "DirectX9 Example"

	WNDCLASSEX wc = { sizeof(WNDCLASSEX), CS_CLASSDC, WndProc, 0L, 0L, GetModuleHandle(NULL), NULL, NULL, NULL, NULL, WINDOW_NAME, NULL };
	RegisterClassEx(&wc);
	HWND hwnd = CreateWindow(WINDOW_NAME, "DirectX 9 Test", WS_OVERLAPPEDWINDOW, 100, 100, 1280, 720, NULL, NULL, wc.hInstance, NULL);

	LPDIRECT3D9 pD3D;
	if ((pD3D = Direct3DCreate9(D3D_SDK_VERSION)) == NULL)
	{
		UnregisterClass(WINDOW_NAME, wc.hInstance);
		return 0;
	}

	ZeroMemory(&g_d3dpp, sizeof(g_d3dpp));
	g_d3dpp.Windowed = TRUE;
	g_d3dpp.SwapEffect = D3DSWAPEFFECT_DISCARD;
	g_d3dpp.BackBufferFormat = D3DFMT_UNKNOWN;
	g_d3dpp.EnableAutoDepthStencil = TRUE;
	g_d3dpp.AutoDepthStencilFormat = D3DFMT_D16;
	g_d3dpp.PresentationInterval = D3DPRESENT_INTERVAL_IMMEDIATE;//D3DPRESENT_INTERVAL_ONE;

	if (IDirect3D9_CreateDevice(pD3D, D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, hwnd, D3DCREATE_HARDWARE_VERTEXPROCESSING, &g_d3dpp, &g_pd3dDevice) < 0)
	{
		IDirect3D9_Release(pD3D);
		UnregisterClass(WINDOW_NAME, wc.hInstance);
		return 0;
	}
	
	D3DADAPTER_IDENTIFIER9 AdapterIdentifier;
	IDirect3D9_GetAdapterIdentifier(pD3D, D3DADAPTER_DEFAULT, 0, &AdapterIdentifier);

	SYSTEM_INFO info;
	GetSystemInfo(&info);
	

	MSG msg;
	ZeroMemory(&msg, sizeof(msg));
	ShowWindow(hwnd, SW_SHOWDEFAULT);
	UpdateWindow(hwnd);

	renderer_initialize(g_pd3dDevice);


	d3d_font_t tahoma;

	init_d3d_font(&tahoma, "Tahoma", 7, FW_BOLD, 0);
	init_device_objects(&tahoma, g_pd3dDevice);
	restore_device_objects(&tahoma);

	DWORD num_fonts = 0;
	AddFontMemResourceEx(font_bin, sizeof(font_bin), 0, &num_fonts);


	d3d_font_t astrium;

	init_d3d_font(&astrium, "AstriumWep", 14, FW_NORMAL, 0);
	init_device_objects(&astrium, g_pd3dDevice);
	restore_device_objects(&astrium);


	while (msg.message != WM_QUIT)
	{
		if (PeekMessage(&msg, NULL, 0U, 0U, PM_REMOVE))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
			continue;
		}

		IDirect3DDevice9_SetRenderState(g_pd3dDevice, D3DRS_ZENABLE, FALSE);
		IDirect3DDevice9_SetRenderState(g_pd3dDevice, D3DRS_ALPHABLENDENABLE, FALSE);
		IDirect3DDevice9_SetRenderState(g_pd3dDevice, D3DRS_SCISSORTESTENABLE, FALSE);

		IDirect3DDevice9_Clear(g_pd3dDevice, 0, 0, D3DCLEAR_TARGET, D3DCOLOR_XRGB(100, 150, 240), 1.0f, 0);
		
		if (IDirect3DDevice9_BeginScene(g_pd3dDevice) >= 0)
		{
			static float time = 0;

			time += 0.05f;

			if (time > 360.f)
				time -= 360.f;

			D3DCOLOR rainbow_color[3];
			
			for (int i = 0; i < 3; i++)
				rainbow_color[i] = color_hsv(time / 360.f + 1.f / (i + 1), 1.f, 1.f, 1.f);

			renderer->begin_draw();


			renderer->draw_line(20, 10, 880, 10, 0xFF000000);

			renderer->draw_box(20, 20, 200, 50, 1, 0xFFFFFFFF);
			renderer->draw_filled_box(240, 20, 200, 50, 0x8000CCCC);
			renderer->draw_gradient_box(460, 20, 200, 50, rainbow_color[0], rainbow_color[1], rainbow_color[2], rainbow_color[3]);
			renderer->draw_box(680, 20, 200, 50, 8, colors_Pink);

			renderer->draw_circle(120, 190, 100, 32, RenderDrawType_Outlined, colors_Red);
			renderer->draw_circle(340, 190, 100, 32, RenderDrawType_Filled, colors_Green);
			//renderer->draw_circle(560, 190, 100, 32, RenderDrawType_Gradient, rainbow_color[0], rainbow_color[1]);
			
			renderer->draw_ring(780, 190, 50, 60, 64, RenderDrawType_Filled, D3DCOLOR_ARGB(255, 165, 50, 200), 0);
			renderer->draw_ring(780, 190, 60, 70, 64, RenderDrawType_Filled, colors_Blue, 0);
			renderer->draw_ring(780, 190, 70, 80, 64, RenderDrawType_Filled, colors_SkyBlue, 0);
			renderer->draw_ring(780, 190, 80, 70, 64, RenderDrawType_Filled, colors_Green, 0);
			renderer->draw_ring(780, 190, 90, 80, 64, RenderDrawType_Filled, colors_Yellow, 0);
			renderer->draw_ring(780, 190, 100, 90, 64, RenderDrawType_Filled, colors_Red, 0);

			renderer->draw_triangle(120, 310, 20, 480, 220, 480, RenderDrawType_Outlined, colors_Green, 0, 0);
			renderer->draw_triangle(340, 310, 240, 480, 440, 480, RenderDrawType_Filled, colors_SkyBlue, 0, 0);
			renderer->draw_triangle(560, 310, 460, 480, 660, 480, RenderDrawType_FilledGradient, rainbow_color[0], rainbow_color[1], rainbow_color[2]);

			renderer->draw_circle_sector(780, 380, 80, 30, time, time + 45, rainbow_color[0], rainbow_color[1]);
			renderer->draw_ring_sector(780, 380, 65, 80, 30, (int)time + 180, (int)time + 225, rainbow_color[2], rainbow_color[2]);

			draw_text(&tahoma, 900, 440, colors_White, "relit privet\npoka relit (", DT_SHADOW);
			//draw_text(&tahoma, 900, 460, colors_White, "relit privet", DT_SHADOW);
			draw_text(&tahoma, 900, 480, colors_White, "relit privet", DT_SHADOW);
			draw_text(&tahoma, 900, 500, colors_White, "relit privet", DT_SHADOW);
			draw_text(&tahoma, 900, 520, colors_White, "relit privet", DT_SHADOW);
			draw_text(&tahoma, 900, 540, colors_White, "relit privet", DT_SHADOW);

			draw_text(&astrium, 350, 50, colors_White,
				("0123456789\nA a B b C c D d\nE e F f H h G g\nI i J j K k L l\nM m N n O o P p\nQ q R r S s\nT t U u V v\n"), DT_SHADOW);

			//text panel
			//if(0)
			//{
			//	static int cpu_usage = 0;
			//	static unsigned last_fps = 0;

			//	const unsigned current_fps = pRender->GetFramerate();

			//	if (last_fps != current_fps)
			//		cpu_usage = GetUsageOfCPU();

			//	last_fps = current_fps;

			//	pRender->DrawString(
			//		900, 10, colors_White, &font1, true, false,
			//		"CPU: %i%%\nFPS: %d\nCPU Cores: %i\n%s",
			//		cpu_usage, current_fps,
			//		info.dwNumberOfProcessors,
			//		AdapterIdentifier.Description);
			//}

			renderer->end_draw();

			IDirect3DDevice9_EndScene(g_pd3dDevice);
		}

		if (IDirect3DDevice9_Present(g_pd3dDevice, NULL, NULL, NULL, NULL) == D3DERR_DEVICELOST &&
			IDirect3DDevice9_TestCooperativeLevel(g_pd3dDevice) == D3DERR_DEVICENOTRESET)
		{
			renderer->on_lost_device();

			if (IDirect3DDevice9_Reset(g_pd3dDevice, &g_d3dpp) >= 0)
				renderer->on_reset_device();
		}
	}

	if (g_pd3dDevice)
		IDirect3DDevice9_Release(g_pd3dDevice);

	if (pD3D) 
		IDirect3D9_Release(pD3D);

	DestroyWindow(hwnd);
	UnregisterClass(WINDOW_NAME, wc.hInstance);

	return 0;
}